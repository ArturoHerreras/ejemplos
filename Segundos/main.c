#include <stdio.h>
#include <stdlib.h>

int main()
{
    int horas , minutos , segundos , total;

    printf( "Introduce la hora en horas, entre las 0 y las 23 , minutos y segundos , separados por espacios:" );

    scanf( "%d %d %d" , &horas , &minutos , &segundos );

    total =(horas*3600)+minutos*60+segundos;

    printf( "El total de segundos que han transcurrido hoy es: %d" , total );

}
