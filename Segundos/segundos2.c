#include <stdio.h>
#include <stdlib.h>

int main()
{
    int horas , minutos , segundos , total;

    printf( "Introduce los segundos que han transcurrido hoy: " );

    scanf( "%d" , &total);

    horas= total/3600;
    minutos = (total-horas*3600)/60;
    segundos = total-horas*3600-minutos*60;

    printf( "La hora actual de hoy es: %d horas %d minutos %d segundos " , horas , minutos , segundos );

}

