#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double f_med_aritmetica(double a , double b , double c , double  d ,  double e ){

   double aritmetica;
   aritmetica = ( a + b + c + d + e)/5;

   return aritmetica;
}

double f_med_cuadratica( double a , double b , double c , double d , double e ){

    double cuadratica;
    cuadratica = (a*a + b*b + c*c + d*d + e*e )/5;

    return cuadratica;

}
double f_med_geometrica( double a , double b , double c , double d , double e ){

    double geometrica;
    geometrica = pow ( (a*b*c*d*e) , (double)1/3);

    return geometrica;

}

double f_med_aritmetica(double a , double b , double c , double  d ,  double e );
double f_med_cuadratica( double a , double b , double c , double d , double e );
double f_med_geometrica( double a , double b , double c , double d , double e );

int main(){

    double a , b , c , d , e , A , B , C;

    printf( "Introduzca , separados por espacios , 5 numeros reales:" );
    scanf( "%lf %lf %lf %lf %lf" , &a , &b , &c , &d , &e );

    A = f_med_aritmetica( a , b , c , d , e );
    B = f_med_cuadratica( a , b , c , d , e );
    C = f_med_geometrica( a , b , c , d , e );


    printf( "Su media aritmetica es %lf\n" , A );
    printf( "Su media  cuadratica es %lf\n" , B );
    printf( "Su media geometrica es %lf\n" , C);


}


