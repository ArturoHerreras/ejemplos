#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int f_inverso(int a);

int main()
{
    int entero;
    printf( "Introduce un numero entero formado por 3 digitos:" );
    scanf( "%d" , &entero );

    printf("Su inverso sera el numero: %d" , f_inverso(entero));
}

int f_inverso(int a){

    int centenas , decenas , unidades , entero2;

     centenas = a/100;
     decenas = (a-centenas*100)/10;
     unidades = (a-centenas*100-decenas*10);
     entero2= unidades*100+decenas*10+centenas;

     return entero2;
}
